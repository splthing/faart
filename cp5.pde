ControlP5 cp5;

Textarea consoleTextArea;
RadioButton inputSelect;
Textfield calibrationRef;
Button setRef;


void cp5Init() {
  PFont font = createFont("arial", 20);

  cp5 = new ControlP5(this);

  calibrationRef = cp5.addTextfield("calibrationref")
    .setPosition(350, 10)
    .setSize(70, 40)
    .setFont(font)
    .setFocus(true)
    ;
  calibrationRef.setValue("94.0");
    
  inputSelect = cp5.addRadioButton("inputselect")
    .setPosition(420,200)
    .setSize(60, 40)
    .setColorForeground(color(120))
    .setColorActive(color(255))
    .setColorLabel(color(255))
    .setItemsPerRow(1)
    .setSpacingColumn(50)
    .setNoneSelectedAllowed(false)
    .addItem("Off", 0)
    .addItem("Input 1", 1)
    .addItem("Input 2", 2)
    ;

  setRef = cp5.addButton ("setref")
    .setPosition(220, 10)
    .setSize(120, 40)
    .setCaptionLabel ("set ref ->")
    .setFont(font)
    ;

  consoleTextArea = cp5.addTextarea("txt")
    .setSize(width-20, 220)
    .setPosition(10, 400)
    .setPosition (10, height-10-220)
    .setFont(createFont("Monaco", 10))
    .setLineHeight(14)
    .setColor(color(0, 255, 1))
    .setColorBackground(color(0, 0, 0.05))
    ;
}


void controlEvent(ControlEvent theEvent) {
  if (theEvent.isFrom(inputSelect)) {
    myPort.write(String.format ("@inputsel=%d\n", (int)inputSelect.getValue()));
  } else if (theEvent.isFrom(setRef)) {
    float newRef = (parseFloat(calibrationRef.getText()));
    myPort.write(String.format ("@dbref=%f\n", newRef));
    dbg("LOCAL: please reset max and L?eq*\n");
  }
}


public void dbg(String text) {
  consoleTextArea.append(text);
  consoleTextArea.scroll(consoleTextArea.getText().length());
}

public void dbg(String[] text) {
  for (int i=0; i<text.length; i++) {
    consoleTextArea.append(text[i]+"\n");
  }
  consoleTextArea.scroll(consoleTextArea.getText().length());
}

public void dbgClear() {
  consoleTextArea.clear();
}