# faart

A Processing sketch to display and log SPL data from esp32-slm project

## Config

Change serial port name and log file in the main processing sketch

## Keyboard Shortcuts

* ```SPACE``` reset max
* ```c``` clear log window
* ```[``` start logging
* ```]``` stop logging

## FAART protocol

A simple serial protocol that allows plain text logging and parameter set/get  
The message structure is:  

```[messageID][paramName]=[paramValue]\n```

Any lines not starting with a message ID will be sent to the log window.

### Parameter Value (ESP32->PC)
messageID = ```*```  
The current value of some parameter in the ESP. These can be requested specifically
or just auto-sent (the ESP sends out SPL measurements like this constantly).
parameter names are stuff like PeakC, PeakFS, SPLZF, SPLAF etc.

### Set parameter (PC->ESP32)
messageID = ```@```  
Sets or requests a parameter in the ESP.  
If no ```=paramValue``` is sent, it's considered a request for that value.

| paramName | paramValue | what |
| ---|---|---|
|inputsel| int| 0=off 1=input1 2=input2|
|dbref| float | set the reference SPL level

examples:  
```@dbref=95.5\n``` sets the current input level as 95.5dB
```@inputsel\n``` requests the currently selected input channel

THIS DOESNT DO REQUESTS YET!!!
